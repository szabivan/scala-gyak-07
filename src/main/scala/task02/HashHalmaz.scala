package task02

/**
 * A task: implementáljunk egy funkcionális generikus HashHalmaz[T] -t.
 *
 * Egy ilyen adatstruktúrában
 * - T típusú objektumokat tárolunk
 * - "halmaz", tehát mindből max egy van az adatszerkezetben
 * - használjuk a T típusú objektumok hashCode metódusát (ez a metódus minden osztályban létezik, egy intet ad vissza)
 * - a HashHalmaz egy hashVector: Vector[Vector[T]]-ben tárolja az adatot, a külső vektornak van egy size: Int mérete
 * - beszúráskor: a beszúrandó objektum hashCode-ját kiszámoljuk, vesszük mod size (0 és size-1 közt legyen az értéke;
 *   vigyázat: a hashCode adhat negatívat is), és ha így az `i` indexet kapjuk, akkor a hashVector[i]-ben először is
 *   megnézzük, hogy ott van-e már a beszúrandó érték, ha már benne van, akkor nem változtatunk semmit, ha nincs,
 *   akkor ennek a hashVector[i]-nek a végére szúrjuk be;
 * - ha beszúrás után a hashVector-beli celláknak több, mint a 3/4-ében nemüres az ott levő vektor, akkor
 *   egy kétszeres méretű hashHalmaz-t építük, beleszúrjuk az elemeket ebből a vektorból, majd megint ellenőrizzük
 *   az elemszámot és addig bővítünk duplázásokkal, amíg már legfeljebb a 3/4-e nemüres az entryknek
 * - contains: megnézzük, hogy a hashCode-nak megfelelő vektorban szerepel-e a lekérdezett érték
 * - remove: úgy vesszük ki a vektorból, ha benne van egy elem, hogy a vektor utolsó elemével updateljük
 *   a törlendő pozíciót, majd eldobjuk a vektor utolsó elemét.
 *
 * - foreach, map, foldLeft a szokásos módon, járja be az elemeket úgy, hogy először a 0. vektort balról jobbra,
 * majd az 1. vektort, ..., végül a size - 1. vektort.
 *
 * Ezekhez helper függvényeket is implementálunk.
 *
 * A scala API doksiból érdemes lehet a Vector[T] osztály
 * +:(T): Vector[T], :+(T): Vector[T]
 * updated(Int,T): Vector[T]
 * dropRight(Int): Vector[T]
 * contains, filter
 * metódusait megnézni hozzá
 */

object VectorUtils {
  /**
   * Implementáljunk egy getIndexFrom függvényt, mely kap egy v vektort, egy e értéket és egy i indexet,
   * és visszaadja Some(j)-t, ha j az első olyan index, melyre i <= j és v(j)=e, és None-t, ha nincs ilyen index!
   * (azaz, ha az érték nem szerepel i utáni indexen)
   *
   *
   */
  def getIndexFrom[T](v: Vector[T], e: T, i: Int): Option[Int] =
    if (i < 0) //0-tól kezdődik az indexelés
      getIndexFrom(v, e, 0)
    else if (i >= v.size) // akkor nincs benne
      None
    else if (v(i) == e) //akkor ez lesz az az index
      Some(i) // Option-ba csomagolva visszaadjuk
    else
      getIndexFrom(v, e, i + 1) //lépünk tovább jobbra

  /**
   * Implementáljunk egy getIndex függvényt, mely kap egy v vektort és egy e értéket, és visszaadja Some(i)-t, ha
   * i az első olyan index, melyre v(i)=e, és None-t, ha nincs ilyen index! (azaz ha az érték nem szerepel a vektorban)
   */
  def getIndex[T](v: Vector[T], e: T): Option[Int] =
    getIndexFrom(v, e, 0) //az első ilyen elem az az első 0-nál nagyobb-egyenlő indexű ilyen elem

  /**
   * Implementáljunk egy removeSwap függvényt, mely kap egy v vektort és egy e értéket, és
   * - ha v-ben nem szerepel e, akkor visszaadja az eredeti v-t,
   * - ha szerepel benne, akkor az *első* előfordulás helyére beteszi a vektor utolsó elemét, levágja az utolsó elemet
   * (így effektíve kitörli az érték első előfordulását a vektorból), és visszaadja az eredményként kapott új vektort!
   * (van egy dropRight metódus, ami ehhez hasznos lehet.)
   */
  def removeSwap[T](v: Vector[T], e: T): Vector[T] = {
    val indexOption = getIndex(v, e) //itt van az első eredetileg
    indexOption match {
      case None => v //nem szerepel, visszaadjuk az eredeti v-t
      case Some(index) => 
        v.updated(index, v(v.size - 1)) //a helyére az utolsó elemet tesszük
        .dropRight(1) //eldobjuk az utolsó 1 elemet
    }
    // foldLeft is jó, akkor nem kell matchelni:
    // indexOption.foldLeft[Vector[T]](v)((v,index) => v.updated(index, v(v.size - 1)).dropRight(1))
  }    
}

  /**
   * A HashHalmaz osztályunk: egy case class, van benne egy vektor és tudja a "külső" vektor méretét.
   * Tartozik hozzá egy companion object.
   */
case object HashHalmaz {
    /**
     * Implementáljuk az apply metódust úgy, hogy a HashHalmaz[T] hívás egy üres, size = 8 mérettel inicializált
     * külső vekttorral térjen vissza! Tehát egy
     * Vector(Vector(),Vector(),Vector(),Vector(),Vector(),Vector(),Vector(),Vector())-al legyen a data mező inicializálva.
     */
    def apply[T](): HashHalmaz[T] = apply(8) // a lentebbi függvény pont ezt csinálja meg size = 8-cal
      
    /**
     * Implementáljuk az apply(size: Int) metódust úgy, hogy a HashHalmaz[T] hívás egy üres, size mérettel inicializált
     * külső vekttorral térjen vissza! Tehát egy
     * Vector(Vector(),...Vector())-al, összesen size darab Vector()-ral, legyen a data mező inicializálva.
     */
    def apply[T](size: Int): HashHalmaz[T] =
      HashHalmaz(
        (1 to size) // 1..size minden elemére
        .map(i => Vector()) // létrehozunk egy vektort
        .toVector //az eredményt egy vektorba tesszük, ha nem abban lenne
        ,
        size) 
}

case class HashHalmaz[T](data: Vector[Vector[T]], size: Int) {
    /**
     * Implementáljuk a loadFactor metódust, mely visszaadja, hogy a data vektor elemeinek mekkora része
     * nemüres vektor! (tehát ez egy 0 és 1 közti Double érték lesz.)
     */
    def loadFactor: Double = 
      data.count( _.nonEmpty ) //megszámoljuk h mennyi a nemüres
      .toDouble / //egészosztás elkerülése végett castoljuk
      data.size //osztva a méretével
      
    /**
     * Implementáljuk az offset metódust, mely megkap egy e értéket,
     * - veszi az e hashCode értékét
     * - ennek a size-al vett osztási maradékát (pl. ha size 8, és hashCode = 15, akkor ez az érték 7; ha hashCode = -3, akkor ez az érték 5)
     * ami így tehát 0..size-1 közti Int lesz,
     * és ezt az intet adja vissza!
     */
    def offset(e: T): Int =
      ((e.hashCode % size) + // most még lehet negatív is
        size) % size   // most már nem: -size..size volt, 0..2size lett, mod size az már nemnegatív osztási maradék
    /**
     * Implementáljuk a contains(e: T): Boolean metódust, mely
     * - veszi az e objektum hashCode értékét
     * - ennek a size-al vett osztási maradékát (pl. ha size 8, és hashCode = 15, akkor ez az érték 7; ha hashCode = -3, akkor ez az érték 5)
     * - és a data ennyiedik vektorában keresi meg az e értéket, és lejelenti, tartalmazza-e!
     */
    def contains(e: T): Boolean = {
      val o = offset(e) //ebben a hash entryben keressük
      data(o) contains e //Vectorban van contains
    }

    /**
     * Implementáljuk az insert(e: T): HashHalmaz[T] metódust, mely a contains-nak megfelelő helyre szúrja az e értéket:
     * - veszi az e objektum hashCode értékét
     * - ennek a size-al vett osztási maradékát
     * - ha a data ennyiedik vektorában már szerepel az e érték, akkor nem készít új példányt, visszaadja módosítás nélkül az adatszerkezetet,
     * - ha még nem szerepel, akkor beszúrja ennek a vektornak a végére (funkcionálisan),
     * - ezen kívül ha egy üres vektorba szúrt be most értéket, akkor még a resizeIfNeeded-et is hívja az eredményre,
     * - és visszaadja az új HashHalmazt!
     */
    def insert(e: T): HashHalmaz[T] = {
      val o = offset(e) //ennyiedik vektorban kéne lennie
      val oldRow = data(o) //ez az ennyiedik vektor
      if (oldRow contains e) this //ha már benne volt, nincs tennivaló
      else {
        val newRow = oldRow :+ e // beszúrtuk
        val newData = data.updated(o, newRow) // updateljük az ennyiedik vektort a régiről az újra
        val newHashHalmaz = HashHalmaz(newData, size)
        if (oldRow.isEmpty) newHashHalmaz.resizeIfNeeded else newHashHalmaz
      }
    }

    /**
     * Implementáljuk a resizeIfNeeded metódust, mely
     * - ha a data -beli nemüres vektorok aránya 0.75 fölött van, akkor
     *   - létrehoz egy új HashHalmaz-t, kétszer akkora size-al, mint az aktuális,
     *   - ebbe beszúrja az aktuális halmazunk összes elemét,
     *   - és az eredményre is hívja a resizeIfNeeded metódust, ennek az eredményét adja vissza!
     * - ha pedig 0.75 vagy kisebb, akkor csak visszaadja módosítás nélkül az adatszerkezetet!
     */
    def resizeIfNeeded: HashHalmaz[T] = {
      if (loadFactor > 0.75) { // túl sok nemüres vödör van
        val newHashHalmaz = HashHalmaz[T](size * 2) //létrehoztunk egy kétszer annyi vödröst
        data
        .foldLeft[HashHalmaz[T]](newHashHalmaz //kiindulunk egy üres halmazból, ráfoldoljuk inserttel az összes elemét
        )(
        (acc, dataRow) => dataRow.foldLeft(acc)( (acc2, value) => acc2.insert(value) ) //tehát minden sor összes elemét
        )
      } else this //ha kisebb, visszaadjuk az adatszerkezetet
    }

    /**
     * Implementáljuk a remove(e: T): HashHalmaz[T] metódust, mely
     * - veszi az e objektum hashCode értékét
     * - ennek a size-al vett osztási maradékát
     * - ha a data ennyiedik vektorában nem szerepel az e érték, akkor visszaadja a hashhalmazt módosítás nélkül,
     * - egyébként pedig az e első előfordulását kiveszi ebből a vektorból oly módon, hogy a hátsót szúrja be a helyére,
     * - elkészít egy új hashhalmazt ezzel a módosított tartalommal,
     * és ezt adja vissza!
     */
    def remove(e: T): HashHalmaz[T] = {
      // hashcode osztási maradékára volt az offset függvény
      val o = offset(e)
      val oldDataRow = data(o) // ebben a vektorban kéne legyen e
      if (oldDataRow.contains(e)) { //ha szerepel benne, vegyük ki, új hashsetet adjunk vissza
        val newDataRow = VectorUtils.removeSwap(oldDataRow, e) //ezért van az a függvény
        val newData = data.updated(o, newDataRow)
        HashHalmaz( //visszaadjuk az új hashhalmazt
          newData,
          size
        )
      } else this //ha nem szerepel benne, ne tegyünk semmit
    }

    /**
     * Implementáljunk egy foreach metódust, ami sorban végigmegy az i=0..size-1-edik vektorokon a data-n belül,
     * ezeknek mindnek az elemein és végrehajtja mindegyik elemen a kapott f függvényt!
     */
    def foreach[U](f: T => U): Unit =
      for {
        dataRow <- data
        item <- dataRow
      } f(item)

}
