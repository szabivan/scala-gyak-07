package zh

/*
 * A ZH task: A típusú dolgok "inventory"-ját implementáljuk, ami lehet:
 * - egy SingleItem, amiben egyetlen A típusú elem van a data adattagban
 * - vagy egy Chest, amiben Inventory[A] elemek vektora van az items adattagban
 * Pl. ha String-eket tárolunk, akkor Inventory[String] pl.
 * - SingleItem("Fakard")
 * - SingleItem("Zúzottkő")
 * - Chest(Vector(SingleItem("Fakard"), SingleItem("Zúzottkő"))) (így ebben a chestben két SingleItem szerepel)
 * - Chest(Vector(SingleItem("Fakard"), Chest(Vector(SingleItem("Zúzottkő"), SingleItem("Bőrpáncél"))), SingleItem("Smaragd")))
 *   (így ebben a chestben három "inventory" szerepel, ebből kettő SingleItem, egy pedig egy újabb Chest, amiben két SingleItem szerepel)
 * Implementáljuk a trait-ben megadott metódusokat! Célszerű lehet ezeket overrideolni a két case classban, de nem kötelező.
 *
 * A stílus pontokra ismét a múltkori irányelvek vonatkoznak (ne legyen mutable field, var, ne értékeljünk ki egy kifejezést többször,
 * ha valamire van függvény (pl a Vector osztályban), akkor használjuk azt, és ne írjuk újra).
 **/

trait Inventory[A] {
  /* Az Inventory mérete legyen a benne összesen szereplő SingleItem-ek száma.
   * Az utolsó példa (a chestben a chest) esetében pl. ez 4 kellene legyen.
   **/
  def size: Int = ???
  
  /* A foreach metódus értékelje ki az argumentumként kapott f függvényt minden olyan datán, ami az Inventory-ban lévő
   * SingleItem-ekben szerepel! Balról jobbra, tehát pl. egy Chest-ben először a Chest első elemében lévő összes SingleItem-re,
   * majd a következő elemében lévő összes SingleItem-re stb.
   * Az utolsó példa esetén pl a foreach(println) hívás eredményeképp konzolra ki kell kerüljön előbb a
   * Fakard, majd a Zúzottkő, majd a Bőrpáncél, végül a Smaragd.
   */
  def foreach[B](f: A => B): Unit = ???
  
  /* A map metódus ha kap egy f: A => B függvényt, akkor hozzon létre egy új Inventory[B]-t, amiben
   * a Chest/SingleItem struktúra ugyanaz, mint az eredetiben, a SingleItem-ek datáit az eredeti daták f melletti képe adja!
   * Pl. az utolsó példa esetén a map(name => name.length) egy Inventory[Int]-et hozzon létre,
   * mégpedig a Chest(Vector(SingleItem(6), Chest(Vector(SingleItem(8), SingleItem(9))), SingleItem(7))) objektumot.
   */
  def map[B](f: A => B): Inventory[B] = ???
  
  /* A count metódus adja vissza, hogy hány olyan SingleItem van az inventoryban, melynek datájára igaz a p predikátum!
   * Pl. az utolsó példa esetén a count(name => name.length < 8) eredménye 2 kéne legyen.
   * 
   **/
  def count(p: A => Boolean): Int = ???
  
  /* A foldLeft metódus kapjon egy kezdőértéket és egy aggregátor függvényt, és "balról jobbra" alkalmazza a függvényt sorban az összes
   * data értékkel, amik az inventory-beli SingleItem-ekben vannak!
   * Pl. az utolsó példa esetén a fold[String]("content:")((acc,data) => acc + " " + data) hívás eredménye
   * "content: Fakard Zúzottkő Bőrpáncél Smaragd" kéne legyen.
   **/
  def foldLeft[B](base: B)(op: (B,A) => B): B = ???
  
  /* A getContents metódus adja vissza az Inventory-ban lévő összes SingleItem datáinak a vektorát, balról jobbra!
   * Pl. az utolsó példa esetén a getContents eredménye
   * Vector("Fakard", "Zúzottkő", "Bőrpáncél", "Smaragd") kéne legyen.
   * Vector-ok összefűzésére használhatjuk pl. a Vector osztály ++ metódusát.
   **/
  def getContents: Vector[A] = ???
  
  /* A removeChests metódus adjon vissza egy olyan Inventory-t, ami ugyanazokat a datákat tartalmazza SingleItem-ekben,
   * mint amiket az eredeti Inventory, de
   * - ha csak egyetlen data van az eredetiben, akkor egyetlen SingleItem-ben adja azt vissza,
   * - ha pedig nem, akkor egyetlen Chest tartalmazza az összes datát egy-egy SingleItem-ben!
   * Pl. az utolsó példán hívva a removeChests eredménye
   * Chest(SingleItem("Fakard"), SingleItem("Zúzottkő"), SingleItem("Bőrpáncél"), SingleItem("Smaragd")) kéne legyen.
   **/
  def removeChests: Inventory[A] = ???

}

case class SingleItem[A](data: A) extends Inventory[A] {
  
}

case class Chest[A](items: Vector[Inventory[A]]) extends Inventory[A] {
  
}

/*
 * Ha akarsz, ide implementálhatsz a saját kódod tesztelésére bármit
 **/
object Zh extends App {

}
