package task01

trait BetuFa {
  def weight: Int
}
case class BetuLevel(ch: Char, weight: Int) extends BetuFa

object BetuSzamol extends App {
  /**
   * Adjunk vissza egy Map[Char,Int]-et, ami az input String-beli betűkből számolja
   * meg, melyikből mennyi van!
   * pl
   * input: "aabaabcab"
   * output: Map(a -> 5, b -> 3, c -> 1), minden más 0
   */
  def countLetters(text: String): Map[Char, Int] = ???
  
  /**
   * Írjunk függvényt, ami egy Map[Char,Int]-ből készít egy Set[BetuFa]-t,
   * amiben minden egyes (c,w) map entryből egy BetuLevel(c,w) készül, amit beleszúrunk!
   */
  def FaSetEpit(freqs: Map[Char,Int]): Set[BetuFa] = ???

}
