package task01

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

import scala.util.Random

class BetuSzamolTeszt extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }
  
  "Betűszámoló teszt" should "random inputra" in {
    def generateString() = (1 to 100).map( _ => (Random.nextInt(10) + 'a').toChar ).mkString("")
    for( _ <- 1 to 1000 ) {
      val input = generateString()
      val result = BetuSzamol.countLetters(input)
      for( off <- 0 to 11 ) {
        val char: Char = (off + 'a').toChar
        assert( result.getOrElse(char,0) == input.count( _ == char ), s"bad output for: ${input}" )
      }
    }
  }
  
  "Fapont építő teszt" should "random inputra" in {
    for( _ <- 1 to 1000 ){
      val (input, expected) = (0 to 10).foldLeft[(Map[Char,Int],Set[BetuFa])] ( Map(), Set() )(
        (acc, i) => { val ch = ('a'+i).toChar; val count = Random.nextInt(10);
          (acc._1.updated( ch, count ), acc._2 + BetuLevel(ch,count))
      } )
      val result = BetuSzamol.FaSetEpit(input)
      assert( result == expected, s"Bad output: $input")
    }
  }
}
