package zh

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

import scala.util.Random

class SizeTest extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }
  
  "SizeTest" should "for a SingleItem" in {
    assert(SingleItem(1).size == 1, "SingleItem(1) mérete 1 kellene legyen!")
  }
  
  it should "for an empty chest" in {
    assert(Chest(Vector()).size == 0, "Chest(Vector()) mérete 0 kellene legyen!")
  } 
  
  it should "for a single-leveled chest" in { 
    val input = Chest(Vector(SingleItem(1),SingleItem(1),SingleItem(2),SingleItem(3)))
    assert(input.size == 4, s"$input mérete 4 kellene legyen!")
  }
  
  it should "for a multi-leveled chest" in {
    val subchest = Chest(Vector(SingleItem(1),SingleItem(2)))
    val input = Chest(Vector(subchest, SingleItem(3), subchest))
    assert(input.size == 5, s"$input mérete 5 kellene legyen!")
  }
}

