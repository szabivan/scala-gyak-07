package zh

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

import scala.util.Random

class MapTest extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }
  
  "MapTest" should "for a SingleItem" in {
    val input = SingleItem(1)
    val result = input map { _ * 2 }
    val expected = SingleItem(2)
    assert(result == expected, s"$input map { _ * 2 } eredménye $result lett, $expected kéne legyen!")
  }
  
  /* it should "for an empty chest" in {
    val input = Chest[Int](Vector())
    val result = input map { _ * 2 }
    val expected = input
    assert(result == expected, s"$input map { _ * 2 } eredménye $result lett, $expected kéne legyen!")
  } */
  
  it should "for a single-leveled chest" in { 
    val input = Chest(Vector(SingleItem(1),SingleItem(1),SingleItem(2),SingleItem(3)))
    val result = input map { _ * 2 }
    val expected = Chest(Vector(SingleItem(2),SingleItem(2),SingleItem(4),SingleItem(6)))
    assert(result == expected, s"$input map { _ * 2 } eredménye $result lett, $expected kéne legyen!")
  }
  
  it should "for a multi-leveled chest" in {
    val subchest = Chest(Vector(SingleItem(1),SingleItem(2)))
    val input = Chest(Vector(subchest, SingleItem(3), subchest))
    val result = input map { _ * 2 }
    val expected = Chest(Vector(Chest(Vector(SingleItem(2),SingleItem(4))), SingleItem(6), Chest(Vector(SingleItem(2),SingleItem(4)))))
    assert(result == expected, s"$input map { _ * 2 } eredménye $result lett, $expected kéne legyen!")
  }
}

