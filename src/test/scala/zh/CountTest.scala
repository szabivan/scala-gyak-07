package zh

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

import scala.util.Random

class CountTest extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }
  
  "CountTest" should "for a SingleItem" in {
    val input = SingleItem(1)
    val result = input count { _ % 2 == 0 }
    val expected = 0
    assert(result == expected, s"$input count { _ % 2 == 0 } eredménye $result lett, $expected kéne legyen!")
  }
  
  it should "for an empty chest" in {
    val input = Chest[Int](Vector())
    val result = input count { _ % 2 == 0 }
    val expected = 0
    assert(result == expected, s"$input count { _ % 2 == 0 } eredménye $result lett, $expected kéne legyen!")
  } 
  
  it should "for a single-leveled chest" in { 
    val input = Chest(Vector(SingleItem(1),SingleItem(1),SingleItem(2),SingleItem(3)))
    val result = input count { _ % 2 == 0 }
    val expected = 1
    assert(result == expected, s"$input count { _ % 2 == 0 } eredménye $result lett, $expected kéne legyen!")
  }
  
  it should "for a multi-leveled chest" in {
    val subchest = Chest(Vector(SingleItem(1),SingleItem(2)))
    val input = Chest(Vector(subchest, SingleItem(3), subchest))
    val result = input count { _ % 2 == 0 }
    val expected = 2
    assert(result == expected, s"$input count { _ % 2 == 0 } eredménye $result lett, $expected kéne legyen!")
  }
}

