package zh

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

import scala.util.Random

class ForeachTest extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }
  
  def foreachTest[A](inventory: Inventory[A]): String = {
    var buffer = new StringBuilder()
    val result = for(data <- inventory) { buffer.append(data.toString) }
    buffer.toString
  }
  
  "ForeachTest" should "for a SingleItem" in {
    val input = SingleItem(1)
    val result = foreachTest(input)
    val expected = "1"
    assert(foreachTest(input) == expected, s"$input.foreach(print) eredménye $result lett, $expected kéne legyen!")
  }
  
  /* it should "for an empty chest" in {
    val input = Chest(Vector())
    val result = foreachTest(input)
    val expected = ""
    assert(foreachTest(input) == expected, s"$input.foreach(print) eredménye $result lett, $expected kéne legyen!")
  }
  
  it should "for a single-leveled chest" in { 
    val input = Chest(Vector(SingleItem(1),SingleItem(1),SingleItem(2),SingleItem(3)))
    val result = foreachTest(input)
    val expected = "1123"
    assert(foreachTest(input) == expected, s"$input.foreach(print) eredménye $result lett, $expected kéne legyen!")
  } */
  
  it should "for a multi-leveled chest" in {
    val subchest = Chest(Vector(SingleItem(1),SingleItem(2)))
    val input = Chest(Vector(subchest, SingleItem(3), subchest))
    val result = foreachTest(input)
    val expected = "12312"
    assert(foreachTest(input) == expected, s"$input.foreach(print) eredménye $result lett, $expected kéne legyen!")
  }
}

